import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
    blog = [
        {
            title: 'Super article',
            content: 'Lorem',
            loveIts: 1,
            created_at: Date.now()
        },
        {
            title: 'Mega article',
            content: 'Lorem',
            loveIts: 3,
            created_at: Date.now()
        },
        {
            title: 'article plutôt nul',
            content: 'Lorem',
            loveIts: -1,
            created_at: Date.now()
        }
    ];

    constructor() {
    }
}
