import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
    @Input() title: string;
    @Input() content: string;
    @Input() loveIts: number;
    @Input() created_at: Date;

  constructor() {}

  ngOnInit() {
  }

    addLove() {
        this.loveIts++;
        console.log(this.loveIts);
    }

    removeLove() {
        this.loveIts--;
        console.log(this.loveIts);
    }
}
